.. _regles_publication:

###########
Publication
###########

Public(s) concerné(s) : Gestionnaire d'application openMairie.

==========================
Livrables et référencement
==========================

Lorsque qu'une version stable et ré-utilisable est prête, il est important de la publier:

Sur la forge publique, ici *Fusion Forge* de l'Adullact:

* la première fois : 

  - ajouter un "onglet utilisateur" nommé ``Site Web sur openmairie.org`` ou ``openmairie.org`` et pointant via un hyperlien vers http://www.openmairie.org/catalogue/*<code du projet>*
  - on peut également ajouter des onglets pour faire le lien vers le forum ou la documentation, mais ils seront redondants avec ceux du portail openMairie
* à chaque version :

  - mettre à jour les élements de l'onglet *Outils de suivi*: bugs, évolutions, feuille de route
  - sous l'onglet Fichiers, ajouter un livrable (ou plusieurs: avec/sans ``dyn``, ``var``, ... ) sous forme d'archive *zip* 

Sur le portail openMairie :

* la première fois :

  - demander à ajouter l'application au catalogue avec un *<code du projet>* à reprendre dans l'hyper-lien de la forge 
  - définir pour cette application
  
    + les hyper-liens vers: la documentation, la démonstration, le livrable, le forum, la feuille de route, la forge, ...
    + le logo
    + les caractéristiques techniques: version courante, type de base, version framework, utilisation PostGIS, ...
* à chaque version :

  - demander à mettre à jour le catalogue et la fiche de l'application: description de l'application, version, ...


================
La documentation
================

Lorsqu'il y a une nouvelle version de l'application et que la version majeure ou mineure est incrémentée, il faut ajouter une nouvelle version de la documentation aussi.

Voici la liste des étapes à reproduire pour une documentation Sphinx dont les sources sont gérées sur GitHub:

* ajouter une nouvelle branche nommée en reprenant la version majeure et mineure de l'application
* dans le fichier ``README.rst`` à la racine des sources de la documentation, modifier la version
* dans le fichier ``source/conf.py``, modifier les variables ``project``, ``copyright``, ``version`` et ``release`` 
* dans le fichier ``source/index.rst``, modifier éventuellement la description de l'application
* dans le menu ``settings`` de GitHub, modifier la branche par défaut pour mettre la nouvelle.
* adapter la documentation à la nouvelle version logicielle

Pour aligner la publication sur ReadtheDocs :

* dans le menu ``admin``, puis ``version``, changer la version par défaut ;
* désactiver les versions ``stable`` et ``latest.``

Lancer une mise à jour de la page de présentation des documentations en appelant l'URL suivante: http://docs.openmairie.org/?refresh

Dans l'application, vérifier que l'hyper-lien de documentation pointe bien vers l'URL de la nouvelle version de documentation :

* dans la méthode ``set_config__footer()`` de la classe applicative
* dans les fichiers ``dyn/footer.inc.php`` ou ``doc/index.php`` si vous les utilisez encore


========================
Le site de démonstration
========================

Lorsqu'il y a une nouvelle version de l'application et que la version majeure ou mineure est incrémentée, il est conseillé de mettre la version de la démonstration à jour aussi.

Voici la liste des étapes à reproduire sur GitLab :

La première fois :

* cloner le dépôt de paramétrage du déploiement des démonstrations https://gitlab.com/openmairie/d.openmairie.org
* dans le fichier ``./demonstration.inc.php``, ajouter un sous-tableau à $demo pour votre application

  - indexer ce sous-tableau avec le même *code projet* que celui utilisé sur le portail
  - renseigner le sous-tableau par analogie avec les autres
* ajouter le fichier ``./demonstration_data/<openmonapplication>.sql``
 
  - ce fichier permet d'exécuter des instructions SQL après le passage du fichier ``install.sql`` pour adapter les données de démonstration
  - on conseille d'y écrire ``DELETE FROM om_droit WHERE libelle='password';`` pour éviter qu'un utilisateur ne modifie les mots de passe et rende la démonstration inutilisable jusqu'au prochain re-déploiment
* Proposer ces modifications par un PULL-REQUEST
* Une fois intégrées au dépôt officiel, ces modifications seront déployer le soir vers 22h

Les fois suivantes :

* mettre à jour ou recloner le dépôt
* mettre à jour les fichiers:

  - ``./demonstration.inc.php``
  - ``./demonstration_data/<monapplication>.sql``
