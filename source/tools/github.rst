##########
Github.com
##########

On décrit ici l'utilisation du portail `GitHub.com <https://github.com>`_ pour gérer les sources sur un dépôt ``git`` d'une documentation en ligne, compilée avec Sphinx, automatiquement publiée sur le site `ReadTheDocs.org <https://readthedocs.org/>`_ indexé par le portail `docs.openmairie.org <https://docs.openmairie.org>`_

Un des objectifs de cette approche est de faciliter la contribution à la documentation. Un utilisateur un peu familier de l'informatique peut facilement ajouter ou modifier du texte grâce à l'éditeur en ligne de GitHub.com et au système des "Pull Request" git. Dès qu'un administrateur du projet valide la propositin, cela déclenche la regénération de la documentation grâce au lien automatique avec readthedocs.org .

Pour pouvoir gérer un projet sur ce site, il faut avoir un compte utilisateur: le bouton "Sign up" de la page d'accueil permet d'obtenir ce compte facilement.

===============
Créer un projet
===============

Public(s) concerné(s) : Administrateur de projet openMairie.

Il s'agit ici, de créer un nouveau repository sur github.com dans l'organisation openmairie. Si vous n'êtes pas membre de cette organisation, vous devez la contacter pour obtenir la création de ce dépôt initial.

Depuis le tableau de bord de github.com, un clic sur le bouton "+" puis "New repository" en haut à droite de l'écran, à côté du login de l'utilisateur, permet d'accéder au formulaire de création d'un dépôt. 

Voici les informations à saisir : 

* Owner : sélectionner "openmairie", pour une meilleure lisibilité du projet 
  tous les dépôts doivent être créés dans cette organisation.

* Repository name : le nom doit être logiciel-documentation sans accents sans
  espaces en minuscules (par exemple pour le logiciel openElec 
  "openelec-documentation" et pour le logiciel openRésultat 
  "openresultat-documentation").

* Description : pour que le dépôt sorte correctement dans les recherches,
  il faut saisir "Documentation Logiciel Sphinx" (par exemple pour openCimetière 
  "Documentation openCimetière Sphinx").

* Public ou Private : sélectionner "Public" puisque les projets openMairie
  sont publics.

* Initialize this repository with a README : ne pas cocher la case.

Puis il suffit de cliquer sur le bouton "Create repository".


===================================================================
Importer la documentation depuis un projet subversion de l'adullact
===================================================================

Public(s) concerné(s) : Administrateur de projet openMairie.

Les pré-requis sont :

* le projet doit déjà être créé sur github.com
* être dans un terminal pour saisir les commandes suivantes
* les commandes : svn, svn2git et git doivent être installées


Étape 0 : Modifier et définir les variables utilisées dans les commandes suivantes ::

    export OLDPRODUCTNAME="openfoncier"
    export NEWPRODUCTNAME="openads"
    export ADULLACTUSER="fmichon"


Étape 1 : Récupération des logs ::
    
    mkdir -p ${NEWPRODUCTNAME}-documentation/${NEWPRODUCTNAME}-documentation && cd ${NEWPRODUCTNAME}-documentation/${NEWPRODUCTNAME}-documentation
    svn log -q svn://scm.adullact.net/scmrepos/svn/${OLDPRODUCTNAME}/documentation > ../LOG
    cat ../LOG | awk -F '|' '/^r/ {sub("^ ", "", $2); sub(" $", "", $2); print $2" = "$2" <"$2">"}' | sort -u > ../authors-transform.txt
    cat ../authors-transform.txt


Étape 2 : MANUEL - Modification des auteurs ::

    => Il faut faire la correspondance entre les utilisateurs de l'adullact et ceux de github
    => exemple : fmichon = Florent Michon <fmichon@atreal.fr>
    => fmichon = login de l'addulact
    => Florent Michon = nom complet du contributeur
    => fmichon@atreal.fr = mail référencé dans github
    vim ../authors-transform.txt    


Étape 3 : Vérification de l'intervalle ::

    export REVS="$(tail -n2 ../LOG|head -n1|awk '{print $1}'|sed "s/r//"):$(head -n2 ../LOG|tail -n1|awk '{print $1}'|sed "s/r//")"
    echo $REVS


Étape 4 : Récupération de tous les commits (très long) ::

    svn2git --authors ../authors-transform.txt --revision $REVS -v svn://scm.adullact.net/scmrepos/svn/${OLDPRODUCTNAME}/documentation


Étape 5 : Import du code sur github ::

    git remote add origin git@github.com:openmairie/${NEWPRODUCTNAME}-documentation.git
    git push -u --all
    git push --tags


Étape 6 : Suppression de l'ancien dépôt de documentation sur l'adullact pour que personne ne committe dessus ::

    svn del -m "Déplacement de la documentation vers Github" svn+ssh://${ADULLACTUSER}@scm.adullact.net/scmrepos/svn/${OLDPRODUCTNAME}/documentation/trunk svn+ssh://${ADULLACTUSER}@scm.adullact.net/scmrepos/svn/${OLDPRODUCTNAME}/documentation/branches
    echo "Documentation déplacée vers https://github.com/openmairie/${NEWPRODUCTNAME}-documentation" > ../MOVED-TO-GITHUB.txt
    svn import -m "Déplacement de la documentation vers Github" ../MOVED-TO-GITHUB.txt svn+ssh://${ADULLACTUSER}@scm.adullact.net/scmrepos/svn/${OLDPRODUCTNAME}/documentation/MOVED-TO-GITHUB.txt


=========================================
Faire l'import initial d'un projet sphinx
=========================================

Public(s) concerné(s) : Administrateur de projet openMairie.

Il n'y a pas de modèle de départ établi actuellement. Une option est de partir de l'arborescence du projet d'une autre application, par exmple :

* ``Readme.rst``
* ``Sources``

  * ``conf.py``
  * ``index.rst``
  * ``guide_techique``
  
    * ``index.rst``
    * ``developement.rst``
    * ``integration.rst``
    
  * ``manuel_utilisateur``
  
    * ``index.rst``
    * ``ergonomie``
    
      * ``index.rst``
      
    * ``fonctionnalites``
    
      * ``index.rst``
      
    * ``parametres``
    
      * ``index.rst``

NB: pour ajouter un répertoire dans le dépôt depuis le portail GitHub, depuis le bouton ``Create New File``, il faut ajouter un fichier dans ce répertoire, et allonger le chemin proposé par le dépôt par exemple : 

* de ``openmonappli-documentation/Sources/manuel_utilisateur/...``
* ajouter ``openmonappli-documentation/Sources/manuel_utilisateur/mon_rep/index.rst``

Il faudra adapter chacun de ces fichiers à l'application concernée, puis mettre en place la compilation sur ReadtheDocs. Les premiers fichiers à mettre à jour sont: ``Readme.rst`` et ``conf.py``. Consultez le paragraphe ':ref:`readthedocs.org`' de cette documentation.


==============================
Contribuer à une documentation
==============================

Public(s) concerné(s) : Contributeur membre du projet openMairie.

Depuis ReadTheDocs, en haut à droite d'une documentation on trouve un lien ``Edit on GitHub``. 

En cliquant sur ce lien, une fois authentifié avec un compte utilisateur GitHub, on arrive sur une fenêtre de visualisaton du fichier ``.rst`` concerné. 

En cliquant sur l'icône "crayon" on peut modifier ce fichier.

Un onglet "Preview" permet de prévisualiser le rendu en interprétation ReST basique. 

Un bouton en bas de formulaire permet de faire un "Commit" dans une branche personnelle.

On peut demander ensuite l'intégration à la branche principale à l'aide d'un "Pull Request". On peut également solliciter un autre contributeur pour une revue. 


