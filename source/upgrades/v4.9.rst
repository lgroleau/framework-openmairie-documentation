##############
La version 4.9
##############


================================
Les nouveautés de la version 4.9
================================

.. note::

    Cette nouvelle version du framework openMairie porte principalement sur la
    réorganisation du code afin de rendre le répertoire core/ autonome. Elle 
    apporte également un domaine de traduction supplémentaire pour permettre 
    aux applications de profiter des traductions du framework ainsi que diverses
    corrections de bugs et améliorations mineures...

Voici la liste des principales améliorations et correctifs apportés 

4.9.0 (20/07/2018)
------------------

* Évolution : Ajout des champs de fusion spécifiques 'titre' et 'corps' permettant la modification
  complète d'une edition utilisant une lettre type ou un état. Ticket #9055.

* Évolution : Ajout du paramètre 'parameters' avec un comportement identique à celui du menu dans les
  sections actions, shortlinks et footer. Ticket #9054.

* Évolution : Suppression de toutes les balises PHP fermantes. Ticket #9053.

* Évolution : Gestion d'un domaine de traduction spécifique pour le framework. En utilisant la
  fonction __( au lieu de _( la cascade domaine de l'application > domaine du framework se fait. Il est
  donc possible de profiter des traductions du framework et en même temps pouvoir surcharger une ou
  plusieurs traductions du framework dans l'application. Ticket #9052.

* Évolution : La configuration du menu, des actions, du footer et des shortlinks de l'application était
  portée uniquement par les scripts 'dyn/<ELEM>.inc.php'. Cette configuration possède désormais trois niveaux
  de paramétrage : instance > application > framework. Ticket #9051.

* Évolution : Ajout du paramètre 'ldap_login_id' pour une nouvelle méthode d'authentification LDAP.
  Ticket #9050.

* Évolution : Création d'un répertoire 'gen/' dans 'core/' qui contiendra les fichiers générés du core.
  Ticket #9049.

* Évolution : Les scripts 'sql/< OM_DB_PHPTYPE >/< TABLE >.inc.php' ne sont désormais plus obligatoires
  si ceux-ci doivent rester vides. Ticket #9048.

* Évolution : Supprimer les deux paramètres 'recherche' et 'selectioncol' de TAB et SOUSTAB afin d'utiliser
  le mécanisme de 'advs_id' pour gérer le même comportement fonctionnel. Ticket #9047.

* Évolution : Ajout d’une condition 'exists()'' sur les actions modifier, supprimer et consulter.
  Ticket #9046.

* Évolution : Suppression de la gestion du titre par la variable 'idz'. Ticket #9045.

* Correction : Support du SSL openStreetMap dans la librairie openLayers afin de corriger le message
  des navigateurs (site non sécurisé). Ticket #9040.

* Correction : Remonte au début du formulaire après l'affichage d'un sous-formulaire. Ticket #9036.

* Correction: Les liens du widget de formulaire 'selecthiddenstaticlick' n'étaient plus fonctionnels.
  Ticket #9029.


4.9.1 (20/11/2018)
------------------
	
* Correction : Les éléments d'ergonomie (saut de page, insertion des sous-états, aide à la saisie des champs de fusion) n'était pas disponible en sous-formulaire pour les objets 'om_lettretype' et 'om_etat'. Ticket #9082.
 	
* Correction : Lorsqu'aucune configuration de base de données n'est présente, une notice PHP était levée sur la page de login. Ticket #9083.
 	
* Correction : Une erreur de base de données dans la méthode de connexion à la base de données affichait une page blanche au lieu d'afficher un message d'erreur propre à l'utilisateur. Ticket #9084.



L'intégralité des modifications sont disponibles dans le fichier HISTORY.txt à 
la racine de l'archive de téléchargement.


.. contents::


==============================================
Mettre à niveau depuis openMairie 4.8 vers 4.9
==============================================

Mettre à jour les références externes
-------------------------------------

Mettre à jour le contenu du fichier :file:`EXTERNALS.txt` à la racine du projet, et activer ces nouvelles références externes comme indiqué `dans le chapitre sur SVN <../tools/svn.html#externals>`_. 


Mettre à jour la base de données
--------------------------------

La structure de la base de données d'openMairie n'a pas changée depuis la version 4.8.0. Le script SQL :file:`core/data/pgsql/v4.9.0.sql` est donc vide.


Lancer une regénération complète
--------------------------------

Cette nouvelle version comprend des modifications du générateur. Une regénération complète est nécessaire pour le bon fonctionnement de la nouvelle version.

Les fichiers générés pour les tables du framework ``om_xxxx`` étant désormais stockés dans le répertoire ``core/gen/`` , vous pouvez supprimer ces fichiers inutiles de votre répertoire ``gen/``.

Modifier le paramétrage de votre catalogue applicatif de traduction, pour que la fonction __() désormais utilisée par le générateur dans les fichiers de ``gen/`` bénéficie toujours de vos traductions: 

* ouvrir votre fichier ``locales/fr_FR/LC_MESSAGES/openmairie.po`` avec PoEdit
* dans le menu ``Catalogue > Configuration > Mots clé`` , Ajouter une ligne avec ``__`` puis valider
* enregistrer le fichier ``.po``


Adaptations du code pour suivre la philosophie OM-4.9
-----------------------------------------------------

NB : Si vous avez surchargé ``dbform``, vous souhaitez a priori que le générateur étende les classes métier générées depuis cette surcharge. Il faut alors ajouter la déclaration suivante dans le fichier ``gen/dyn/gen.inc.php`` :

  .. code-block :: php

    /**
    * Surcharge applicative de la classe 'om_dbform'.
    */
   	$om_dbform_path_override = "../obj/om_dbform.class.php";
   	$om_dbform_class_override = "om_dbform";

Vous pouvez migrer la classe ``utils`` vers une classe homonyme de l'application : 

- remplacer dans ``app/index.php`` l'instanciation de la classe ``utils`` par celle d'une classe ``monapplication`` en adaptant le nom du fichier inclus, et le nom de la classe instanciée
- renommer le fichier ``obj/utils.class.php`` en ``obj/monapplication.class.php`` et modifier la déclaration de classe pour indiquer ``monapplication`` au lieu de ``utils``

Vous pouvez de plus :

- Supprimer les balises PHP fermantes ``?>`` de la fin de vos fichier de surcharge
- Supprimer ``dyn/include.inc.php`` s'il était toujours présent
- Supprimer les fichiers ``sql/pgsql/<table>.inc.php`` qui n'apportent aucune surcharge
- Transférer vos surcharges applicatives contenues dans les fichiers ``dyn/actions.inc.php``, ``dyn/footer.inc.php``, ``dyn/menu.inc.php``, ``dyn/shortlinks.inc.php`` vers la classe ``framework_openmairie`` en surchargeant les fonctions dédiées ``set_config__actions()``, ... et supprimer les fichiers obsolètes de ``dyn/``
- Limiter les traductions dans le fichier ``.locales/fr_FR/LC_MESSAGES/openmairie.po`` à celles de votre application 

  + utiliser dans vos surcharges PHP la fonction ``__($msgid, $msgstr_default = "")`` au lieu de ``_($msgid)`` qui fait appel à la traduction "framework" sauf si vous la surchargez dans le fichier ``.po`` applicatif ou lors de l'appel ($msgstr_default).
  + adapter les préférences du catalogue du fichier ``.po`` applicatif en excluant les répertoires de source pris en charge par le ``.po`` du core

    - ``core/``
    - ``gen/obj/om*``
    - ``gen/sql/pgsql/*``
    - ``lib/``
    - ``php/``
  + adapter les préférences du catalogue du fichier ``.po`` applicatif en ajoutant aux `mots-clés source` la fonction ``__``
  
    

