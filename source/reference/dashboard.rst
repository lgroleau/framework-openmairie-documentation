.. _dashboard:

##################
Le tableau de bord
##################

.. contents::

========
Principe
========

Le tableau de bord est la page d'accueil de l'utilisateur. Il permet de lui afficher des blocs d'informations contextualisées. Ces blocs sont appelés des widgets. Un tableau de bord est paramétrable par profil.


======
widget
======

Le widget (WIDGET DASHBOARD) est un bloc d'informations contextualisées accessible depuis le tableau de bord de l'utilisateur. Il peut être de type 'Web' ou de type 'Script' et peut gérer toutes sortes d'informations internes ou externes à l'application (les tâches non soldées pour openCourrier, les appels à la maintenance, l'horoscope, la météo, une vidéo, des photos, ...).


-----------------------
Le widget de type 'Web'
-----------------------

Un widget de ce type permet de composer l'affichage du widget avec le contenu du champ texte saisi par l'utilisateur dans l'interface.


--------------------------
Le widget de type 'Script'
--------------------------

Un widget de ce type est disponible pour chaque script PHP présent dans le répertoire ``app`` et préfixé par ``widget_``. Il permet d'afficher dans le bloc widget les éléments rendus dans le script. Il est possible de définir le lien du footer, le libellé du footer et si le widget est vide (le bloc n'est pas du tout affiché).

``app/widget_example.php``

.. code-block:: php

   <?php
   /**
    * WIDGET DASHBOARD - widget_example.
    *
    * L'objet de ce script est de fournir un exemple de widget de type 'Script'.
    *
    * @package openmairie_framework
    * @version SVN : $Id$
    */
   
   // On instancie la classe utils uniquement si la variable $f n'est pas déjà définie
   // pour protéger l'accès direct au script depuis l'URL. La permission "forbidden"
   // a pour vocation de n'être donnée à aucun utilisateur.
   require_once "../obj/utils.class.php";
   if (!isset($f)) {
       $f = new utils(null, "forbidden");
   }
   
   //
   $footer = "";
   
   //
   $footer_title = "";
   
   //
   $widget_is_empty = true;
   
   ?>


-----------------
Modèle de données
-----------------

.. code-block:: sql

    CREATE TABLE om_widget
    (
      om_widget integer NOT NULL, -- Identifiant unique
      libelle character varying(100) NOT NULL, -- Libellé du widget
      lien character varying(80) NOT NULL DEFAULT ''::character varying, -- Lien qui pointe vers le widget (peut être vers une URL ou un fichier)
      texte text NOT NULL DEFAULT ''::text, -- Texte affiché dans le widget
      type character varying(40) NOT NULL, -- Type du widget ('web' si pointe vers une URL ou 'file' si pointe vers un fichier)
      CONSTRAINT om_widget_pkey PRIMARY KEY (om_widget)
    );

- ``obj/om_widget.class.php``
- ``sql/pgsql/om_widget.form.inc.php``
- ``sql/pgsql/om_widget.inc.php``
- ``core/obj/om_widget.class.php``
- ``core/sql/pgsql/om_widget.form.inc.php``
- ``core/sql/pgsql/om_widget.inc.php``
- ``gen/obj/om_widget.class.php``
- ``gen/sql/pgsql/om_widget.form.inc.php``
- ``gen/sql/pgsql/om_widget.inc.php``


====================
Les tableaux de bord
====================

...

-----------------
Modèle de données
-----------------

.. code-block:: sql

    CREATE TABLE om_dashboard
    (
      om_dashboard integer NOT NULL, -- Identifiant unique
      om_profil integer NOT NULL, -- Profil auquel on affecte le tableau de ville
      bloc character varying(10) NOT NULL, -- Bloc de positionnement du widget
      "position" integer, -- Position du widget dans le bloc
      om_widget integer NOT NULL, -- Identifiant du widget
      CONSTRAINT om_dashboard_pkey PRIMARY KEY (om_dashboard),
      CONSTRAINT om_dashboard_om_profil_fkey FOREIGN KEY (om_profil)
          REFERENCES openexemple.om_profil (om_profil),
      CONSTRAINT om_dashboard_om_widget_fkey FOREIGN KEY (om_widget)
          REFERENCES openexemple.om_widget (om_widget)
    );

- ``obj/om_dashboard.class.php``
- ``sql/pgsql/om_dashboard.form.inc.php``
- ``sql/pgsql/om_dashboard.inc.php``
- ``core/obj/om_dashboard.class.php``
- ``core/sql/pgsql/om_dashboard.form.inc.php``
- ``core/sql/pgsql/om_dashboard.inc.php``
- ``gen/obj/om_dashboard.class.php``
- ``gen/sql/pgsql/om_dashboard.form.inc.php``
- ``gen/sql/pgsql/om_dashboard.inc.php``



