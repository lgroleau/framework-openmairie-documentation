 .. _sig:

##################
Module 'MAP'/'SIG'
##################

.. contents::

========
Principe
========

Il est proposé dans ce chapitre de décrire le module sig interne
qui permet la geo localisation d'objet dans openMairie

Depuis la version 4.4.0, le sig interne est accessible dans le framework en mettant
le paramètre option_localisation d'om_parametre à la valeur "sig_interne".

L'objectif de sig interne est de permettre une saisie le plus souvent automatique 
de géométries. Cette saisie est  stockée dans la base métier postgresql.
Elle est affichée sur des fonds existants sur internet : google sat, openStretmap
ou bing (pour l instant). Elle peut être affichée sur un flux (wms ou tiles)

Il n'est donc pas nécessaire de disposer d'un SIG pour utiliser le sig interne;

Le format de stockage des données pgsql est celui de l'OGC et il est accessible aux
clients libres où propriétaires qui respectent ce format. Par exemple qgis (outil libre)
peut accéder aux données de la base postgres "métier".





Affichage de carte
==================

L'affichage se fait avec openLayers dont le composant est de base
dans le framework openMairie : lib/openLayers. (le composant est
installé de manière a être optimisé avec une css openmairie)

La librairie proj4 inclus dans lib/openLayers permet de pouvoir utiliser
les projections lambert sud et lambert 93.

La projection géographique et Mercator est de base dans openLayers

L'enjeu est donc de projeter les données stockées dans la base "métier"
postgresql - postgis (les communes devant utiliser le lambert93) en mercator
pour être lisible avec les cartes accessibles sur internet.

L'affichage des marqueurs est fait au travers d'une requête postgresql
qui alimente un tableau json lu comme une couche openLayers.

La data à modifier est fourni par requete postgresql au format wkt à openLayers.


le sig interne permet ::

    - l affichage de/des  fond(s)
    - l'affichage de marqueurs (data)
    - l affichage du geométries qui peut être créé ou déplacé (couche wkt)

dans la version 4.2.0, il permet  ::

    - l'affichage de flux wms et wfs (getmap) et de recuperer les données (getfeature)
    - la collation de géométrie dans un pannier et son enregistrement en multi géométries

dans la version 4.4.5 il a été rajouté ::

    - la boite à outils : édition du formulaire, édition, outils de mesure, géolocalisation
    - l'outil de saisie a été amélioré
    - les cartouche info, couche et fond
    
Dans cette dernière version, le module est intégré dans openMairie et utilise les formulaires
de saisie, le moteur de recherche et les requêtes mémorisées lors de l'affichage.


L'intégration dans openMairie
=============================

Le module est intégré à openMairie :

Le module SIG est accessible depuis le formulaire d'affichage (tab), par le bouton suivant :

.. image:: sig.png 

Il prend en compte la recherche simple ou avancée du formulaire d'affichage.

Le module SIG est accessible depuis le module de requête mémorisée.

Il est possible d'éditer les attributs d'un objet sélectionné et de les modifier via le formulaire.




Paramétrage de la carte
=======================

Le paramétrage général  des cartes  est modifiable dans  dyn/var_sig.inc ::

    //$cle_google = "xxxxxxxxxxxxxxxxxxxx";
    //$http_google="http://maps.google.com/maps?file=api&amp;v=2&amp;key=";
    //$http_bing='http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&amp;mkt=en-us'; // 6.3c au lieu de 6.2
    //$fichier_jsons="sig_json.php?obj=";
    //$fichier_wkt="sig_wkt.php";
    // *** zoom par couche : zoom standard permettant un passage de zoom a l autre
    //$zoom_osm_maj=18;
    //$zoom_osm=14;
    //$zoom_sat_maj=8;
    //$zoom_sat=4;
    //$zoom_bing_maj=8;
    //$zoom_bing=4;
    // *** popup data contenuHTML
    //$width_popup=200;
    //$cadre_popup=1;
    //$couleurcadre_popup="black";
    //$fontsize_popup=12;
    //$couleurtitre_popup="black";
    //$weightitre_popup="bold";
    //$fond_popup="yellow";
    //$opacity_popup="0.7";
    // *** localisation maj ou consultation
    //$img_maj="../img/punaise_sig.png";
    //$img_maj_hover="../img/punaise_hover.png";
    //$img_consult="../img/punaise_point.png";
    //$img_consult_hover="../img/punaise_point_hover.png";
    //$img_w=14;
    //$img_h=32;
    //$img_click="1.3";// multiplication hauteur et largeur image cliquee

Toutes ces variables ne sont plus accessible dans la version 4.5.0 (à verifier)


Le paramétrage de la projection qui est proposé dans le formualaire de saisie
om_sig_map se paramètre dans var_sig.inc.php.

Il est décrit ci dessous son paramétrage par défaut ::
  
    $contenu_epsg[0] = array("","EPSG:2154","EPSG:27563");
    $contenu_epsg[1] = array("choisir la projection",'lambert93','lambertSud');
        

Il est à noter que les étendues ne sont plus dans var_sig dans la version 4.4.5 et qu'elles
sont stockées dans la table om_sig_extent.


Autre point d'entrée
====================

Il est créé un om_map.class.php dans obj pour pouvoir mettre les points d'entrée

Nous décrivons ici les anciens points d'entrés de form_sig.php et de tab_sig.php

Dans dyn/form_sig_update.inc.php, il est possible de paramétrer
des post traitements de saisie de géométrie

Dans dyn/form_sig_delete.inc.php, il est possible de paramétrer
des post traitements de suppression de géométrie

Dans dyn/tab_sig_barre.inc.php, il est possible de personnaliser la fenêtre

La barre permet de modifier des champs exemple openAdresse

.. image:: OpenAdresse.jpg

La barre permet d'afficher les attributs d'un regard, et permet d'aller sur le point aval ou
le point amont.

.. image:: openReseau.jpg

La barre permet une recherche d adresse dans openFFTH

.. image:: openFTTH.jpg

La barre permet de changer de cimetiere dans openCimetiere

.. image:: openCimetiere.png



=========
Les modes
=========

tab_sig
=======

URL : ``"".OM_ROUTE_MAP."&mode=tab_sig"`` ou ``"../app/index.php?module=map&mode=tab_sig"``

les requêtes de mises à jour du geom ne sont plus initiée en form


les variables get sont les suivantes ::
    
    obj : carte d'om_sig_map sur laquelle on travaille
    idx : numéro d'enregistrement - peut être vide si on part d'une recherche
    popup -> 0 : le menu est affiché; 1 : il ne l'est pas (tableau/options)
    seli : géometrie selectionnée
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options
    advs_id=
    recherche  -> recherche simple  exemple : 02
    retour     -> exemple : tab
    

    
appel librairie : "../core/om_map.class.php"; ::

    new om_map : obj + options
    om_map = new om_map(obj, options);
    om_map->recupOmSigMap();
    om_map->recupOmSigflux();
    om_map->computeFilters(options['idx']);
    om_map->setParamsExternalBaseLayer();
    om_map->prepareCanevas();


form_sig
========

URL : ``"".OM_ROUTE_MAP."&mode=form_sig"`` ou ``"../app/index.php?module=map&mode=form_sig"``

les requêtes de mises à jour du geom ne sont plus initiée en form

variables get ::
    
    obj, idx
    popup -> tableau options
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options
    validation (variable de validation spécifique form)
    
appel librairie : "../core/om_map.class.php"; ::

    New om_map : obj + options
    om_map->recupOmSigMap
    preparation du geojson 
    om_map->prepareForm( min, max, validation, geojson);



map_compute_geom
================

URL : ``"".OM_ROUTE_MAP."&mode=compute_geom"`` ou ``"../app/index.php?module=map&mode=compute_geom"``

En utilisation du pannier, computegeom fait une union des geometries lorsque l on fait une validation (bouton v)
après selection de multiples géométries

variables get ::
    
    obj, idx
    popup -> tableau options
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options


appel librairie : "../core/om_map.class.php"; ::

    om_map = new om_map(obj, options);
    om_map->recupOmSigMap();
    ...
    echo sep.om_map->getComputeGeom(c, geojson[i]);


map_get_filters
===============

URL : ``"".OM_ROUTE_MAP."&mode=get_filters"`` ou ``"../app/index.php?module=map&mode=get_filters"``

gestion du filtre qgis dans om_sig_map_flux

Exemple le père et tous ses fils ::

    SELECT 'fpere_point:²pere² IN ( '||pere||' );fpere_perim:²pere² IN ( '||pere||' );ffils_point:²pere²
    IN ( '||pere||' );ffils_point:²pere² IN ( '||pere||' );ffils_perim:²pere² IN ( '||pere||' )'
    AS buffer FROM ( SELECT array_to_string(array_agg(pere), ' , ') AS pere FROM &DB_PREFIXEpere
    WHERE pere IN (SELECT &idx::integer UNION &lst_idx) ) a


variables get ::
    
    obj, idx
    popup -> tableau options
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options


appel librairie : "../core/om_map.class.php"; ::

    om_map = new om_map(obj, options);
    om_map->recupOmSigMap();
    om_map->recupOmSigflux();
    ...
    om_map->computeFilters(idx_sel);


map_get_geojson_cart
====================

URL : ``"".OM_ROUTE_MAP."&mode=get_geojson_cart"`` ou ``"../app/index.php?module=map&mode=get_geojson_cart"``


Récupére les géométries du pannier

variables get ::
    
    obj, idx
    popup -> tableau options
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options


appel librairie : "../core/om_map.class.php"; ::

    om_map = new om_map(obj, options);
    om_map->recupOmSigMap();
    om_map->recupOmSigflux();

    lst=om_map->getGeoJsonCart(cart, lst);
    ... affichage de la liste

map_get_geojson_markers
=======================

URL : ``"".OM_ROUTE_MAP."&mode=get_geojson_markers"`` ou ``"../app/index.php?module=map&mode=get_geojson_markers"``

Renvoie en format json les marqueurs (ex bulles)

variables get ::
    
    obj, idx
    popup -> tableau options
    min max -> sert à la prépartion du geojson
    etendue -> tableau options
    reqmo -> tableau options
    premier, recherche, selectioncol, adv_id -> tableau options
    valide -> tableau options
    style -> tableau options
    onglet -> tableau options


appel librairie : "../core/om_map.class.php"; ::

    om_map = new om_map(obj, options);
    om_map->recupOmSigMap();
    lst=om_map->getGeoJsonMarkers(options['idx']);
    ... affichage de la liste
    
map_redirection_onglet
======================

URL : ``"".OM_ROUTE_MAP."&mode=redirection_onglet"`` ou ``"../app/index.php?module=map&mode=redirection_onglet"``

Ce programme sert à faire afficher sous form : fenetre dans une fenetre courante

Il appelle utils.class.php
    
Il permet la redirection vers le formulaire de l'objet en visualisation (action=3) si l'objet 
existe et de faire un ajout sinon.

L'ajout ne fonctionne pas.
En cas d'appui sur formulaire, le message apparait  : "aucun enregistrement sélectionné"


export_sig
==========

URL : ``"".OM_ROUTE_TAB."&mode=export_sig"`` ou ``"../app/index.php?module=tab&mode=export_sig"``

ce programme permet de faire un affichage sur la base d'un tab ou d une recherche.

- export suivant le moteur de recherche (equivalent de export csv)

- image dans app/img + app/css (appel à l image)

- modification de core/om_layout.php
  function display_table_global_action 

- pour l instant ou csv ou sig -> pas de possibilité de faire les 2.

- bug en recherche si aucun enregistrement -> ouvre un nouvel onglet


reqmo
=====

A venir.

La modification de programme permet de lancer un tab_sig depuis  reqmo.

Le programme a donc évolué pour gérer cette possibilité mais elle n est pas encore effective (voir alain)

Par contre une modification est obligatoire dans core/om_layout.class.php pour proposer l'option 'json'



Nouvelles images dans img/ et nouvelle css pour l'interface om_sig
==================================================================

Des nouvelles images pour l interface ::

    map-nav.png
    map-geoloc.png
    map-form.png
    map-edit-valid.png
    map-edit-select.png
    map-edit-record.png
    map-edit-point.png
    map-edit-modif.png
    map-edit-get-cart.png
    map-edit-erase.png
    map-edit-draw-regular.png
    map-edit-draw-polygon.png
    map-edit-draw-line.png
    map-edit.png
    map-distance.png
    map-area.png
    
des nouvelles images pour recherche csv ou sig (geojson) ::

    sig.png
    csv.png


Nouvelle css à mettre en layout_jqueryui_before.css

Probleme d'affichage à regler



    

    
    

    
.. _core:

==========================
La classe om_map.class.php
==========================

Nouveauté version om 4.4.0sig

Ce module est dans CORE

Une classe intermédiaire est à créer dans obj/om_map.class.php

Actuellement créée, elle a un problème de prise en compte du zoom au départ BUG



Les variables globales
======================

Elles sont définies ci dessous ::

    var f;
	projections
        var defBaseProjection;
        var defDisplayProjection;
	
	paramètres
        var	obj;
        var	idx;
        var	sql_lst_idx;
        var	idx_sel;
        var	popup;
        var	seli;
        var etendue;
        var reqmo;
        var premier;
        var recherche;
        var selectioncol;
        var tricol;
        var advs_id;
        var valide;
        var style;
        var onglet;
        var type_utilisation = '';

	gestion de l'affichage
        var affichageZones= array();

	gestion de l'enregistrement
        var recordMultiComp; true: enregistrement de l'ensemble des champs géométriques ;
            false: enregistrement un par un des champs géométriques (par défaut)
        var recordMode; 1 (par défaut): via form_sig; 2 retour des valeurs dans des
            champs fournis dans le tableau recordFields
        var recordFields = array(); listes des champs retour (même index que comp)
	
	om_sig_map
        var sm_titre;
        var sm_source_flux;
        var sm_zoom;
        var sm_fond_sat;
        var sm_fond_osm;
        var sm_fond_bing;
        var sm_layer_info;
        var sm_fond_default;
        var sm_projection_externe;
        var sm_retour;
        var om_sig_map;
        var sm_url;
        var sm_om_sql;
        var sm_om_sql_idx;
        var sm_restrict_extent;
        var sm_sld_marqueur;
        var sm_sld_data;
        var sm_point_centrage;
	
	champs geom
        var cg_obj_class = array();
        var cg_maj = array();
        var cg_table = array();
        var cg_champ_idx = array();
        var cg_champ = array();
        var cg_geometrie = array();
        var cg_lib_geometrie = array();

	champs flux
        var fl_om_sig_map_flux = array();
        var fl_m_ol_map = array();
        var fl_m_visibility = array();
        var fl_m_panier = array();
        var fl_m_pa_nom = array();
        var fl_m_pa_layer = array();
        var fl_m_pa_attribut = array();
        var fl_m_pa_encaps = array();
        var fl_m_pa_sql = array();
        var fl_m_pa_type_geometrie = array();
        var fl_m_sql_filter = array();
        var fl_m_filter = array();
        var fl_m_baselayer = array();
        var fl_m_singletile = array();
        var fl_m_maxzoomlevel = array();
        var fl_w_libelle = array();
        var fl_w_attribution = array();
        var fl_w_id = array();
        var fl_w_chemin = array();
        var fl_w_couches = array();
        var fl_w_cache_type = array();
        var fl_w_cache_gfi_chemin = array();
        var fl_w_cache_gfi_couches = array();
	
	champs pour fonds de carte externes (OSM, Bing, Google)
        var pebl_http_google;
        var pebl_cle_bing;
        var pebl_cle_google;
        var pebl_zoom_osm_maj;
        var pebl_zoom_osm;
        var pebl_zoom_sat_maj;
        var pebl_zoom_sa;
        var pebl_zoom_bing_maj;
        var pebl_zoom_bing;

	paramètres de style pour la couche marqueur
        var img_maj="../img/punaise_sig.png";
        var img_maj_hover="../img/punaise_hover.png";
        var img_consult="../img/punaise_point.png";
        var img_consult_hover="../img/punaise_point_hover.png";
        var img_w=14;
        var img_h=32;
        var img_click="1.3";multiplicateur hauteur et largeur image cliquee

	gestion des paniers
        var cart_type = array(
            "point" => false,
            "line" => false,
            "polygon" => false
        );
	tableau de la barre du menu d'édition menu (id html, false)
        var edit_toolbar= array(
            "#map-edit-nav" => false, 
            "#map-edit-draw-point" => false, 
            "#map-edit-draw-line" => false,
            "#map-edit-draw-polygon" => false,
            "#map-edit-draw-regular" => false,
            "#map-edit-draw-regular-nb" => false,
            "#map-edit-draw-modify" => false,
            "#map-edit-draw-select" => false,
            "#map-edit-draw-erase" => false,
            "#map-edit-cart" => false,
            "#map-edit-get-cart" => false,
            "#map-edit-draw-record" => false,
            "#map-edit-draw-delete" => false,
            "#map-edit-draw-close" => false
            );  

Les methodes 
============

Les méthodes sont les suivantes


methode d'initialisation de l objet map::

    construct(obj + options) 
        initialisation des propriétés de l objet om_sig

	Récupération du paramétrage de l'objet dans les tables om_sig_map
    et om_sig_map_comp. Préalable à toute utilisation de la classe
	function recupOmSigMap()
        requete om_sig_map + om_sig_extend 

    Génère un tableau (idx, sql_lst_idx) correspondant aux données idx/Reqmo/Recherche
    le tableau est afficher par la classe om_table
	function getSelectRestrict(idx, seli)
    
 	Génère un tableau GeoJson correspondant aux données idx/Reqmo/Recherche
	function getGeoJsonDatas(idx, seli)   

	Génère un tableau GeoJson correspondant au panier cart (n de flux)
    avec la liste des enregistrement lst 
	function getGeoJsonCart(cart, lst)

	Génère un tableau GeoJson correspondant aux données idx/Reqmo/Recherche
	function getGeoJsonMarkers(idx)
    
	calcul des filtres pour les flux de type WMS (fl_m_filter)
	function computeFilters(idx)
    
	Récupération du paramétrage des flux associés à l'objet dans les tables om_sig_map_flux
        et om_sig_map_flux
    function recupOmSigflux()
        requete dur om_sig_flux
        initialise le paramétrage de flux voir -> "champs flux"
    
	Initialisation des propriétés relatives aux fonds de carte externe,
        ajout des librairies associées si nécessaire
	function setParamsExternalBaseLayer()
        include de var_sig.inc
        initialise les "champs pour fonds de carte externes (OSM, Bing, Google)"
        
	Ecrit les propriétés de l'instance dans la page html pour JavaScript
	function prepareJS( )
        suite d'echo des variables 


Preparation et affichage du canevas ::

	Paramétrage des zones du canevas
	function setCanevas(zone, val) {
		this->affichageZones[zone]=val;
        
	Préparation du canevas html: pilote les autres fonctions prepareCanevas...
	function prepareCanevas( )
        initialisation du tableau this->affichageZones
    
	Préparation du canevas html: menu avec regroupement (au moins une valeur  à 1)
	function prepareCanevasMenu() {    

    Affichage du canevas MODIFIE JLB 
	function prepareCanevasTitre()
    function prepareCanevasEdit()
    function prepareCanevasTools() -> fonction vidée par jlb
    function prepareCanevasInfos() -> fonction vidée par jlb
    function prepareCanevasPrint() -> fonction enlevée par jlb
    function prepareCanevasLayers() -> reprend les 2 fonctions enlevées par jlb
    function prepareCanevasNavigation() -> modif jlb
    function prepareCanevasGetfeatures() -> modif jlb
    
	Calcul la géométrie validé dans l'interface -> appel par map_compute_geom
	function getComputeGeom(seli, geojson)    
 
	Préparation du canevas html: pilote les autres fonctions prepareCanevas...
    -> appel par form_sig.php
	function prepareForm( min, max, validation, geojson)p_init()
    
voir core/obj et core/sql











.. _js:

==============
Le java script
==============

les fonctions sont dans js/sig.js

Nouvelles fonctions ::

    
    affectation d'un mode d'action :
        Géolocalisation, Information,
        Navigation, Edition, Formulaire,
        Mesure surface, Mesure distance
        function map_set_mode_action(mode)
    
    function popupIt pour utilisation dans sig
        function map_popupIt(objsf, link, width, height, callback, callbackParams) {

    implémentation simplifiée de l'utilisation de popupIt dans la fonction SIG
    function map_popup(url
    
    vide les div du div map-getfeatures
    function map_clear_getfeatures()
    
    ouverture du div map-getfeatures 
    function map_close_getfeatures()
    
    affichage dans la zone info
    function map_affiche_info(message)
    
    affichage dans la zone titre
    function map_affiche_titre()
    
    Traitement du select d'un élement de la couche des marqueurs
    function map_on_select_feature_marker(event)
    
    Traitement du unselect d'un élement de la couche des marqueurs
    function map_on_unselect_feature_marker(event)
    
    Traitement du select d'un élement de la couche des datas
    function map_on_select_feature_data(event)
    
    Traitement du unselect d'un élement de la couche des datas
    function map_on_unselect_feature_data(event)
    
    récupère la valeur d'un attribut (traitement GetFeatureInfo)
    function traiteGetFeatureInfoRecAttribut(attributes,name)
    
    récupère la valeur d'un attribut et formate la restitution (traitement GetFeatureInfo)
    function traiteGetFeatureInfoRecAttributFormat(attributes,name,title, formatage)
    
    traitement pour chaque GetFeatureInfo
    function traiteGetFeatureInfo(i,rText,res)
    
    Traitement du clic sur la carte
    function map_clic(e) 
    
    copie les données geoson correspondant à l'idx sélectionné dans les
    couches d'édition correspondantes
    function map_copyDataToEditLayers()
    
    Traitement clic Edit Draw Point
    function map_clicEditDrawPoint()
    
    Traitement clic Edit Draw Line
    function map_clicEditDrawLine()
    
    Traitement clic Edit Draw Polygon
    function map_clicEditDrawPolygon()
    
    //traitement du onChange sur le nombre de côté du polygone régulier
    function map_EditDrawRegularChange()
    
    Traitement clic Edit Draw Polygon
    function map_clicEditDrawRegular()
     
    Traitement clic Edit modify
    function map_clicEditDrawModify()
    
    Traitement clic Edit select
    function map_clicEditSelect()
    
    Traitement clic Edit navigate
    function map_clicEditNavigate()
    
    Traitement clic Edit Erase
    function map_clicEditErase()
    
    test si une valeur est un entier
    function map_is_int(value)
    
    désactive tous les controles d'édition sauf sauf
    function map_deactivate_all_edit_control(sauf)
    
    affiche/cache les éléments de la barre d'outils d'édition
    function map_RefreshEditBar()
    
     calcule et valide les champs géométriques dont les composants sont sélectionnés
    function map_computeGeom(sel, enreg)
    
    traitement du clic sur le bouton de récupération du panier
    function map_clicEditGetCart()
    
    Selection d'un panier
    function map_select_cart(n)
    
    Selection d'un champ d'édition
    function map_select_edit_champ(n)
    
    Traitement géolocalisation
    function map_clicGeolocate()
    
    traitement bouton Edit
    function map_clicEdit()
    
    traitement bouton Edit Close
    function map_clicEditClose()
    
    Traitement de l'application des SLD pour les données geojson
    function map_successGetSld_geojson_datas(req)
    
    Chargement des données aux formats geojson
    function map_load_geojson_datas(bRefreshFilters)
    
    Traitement de l'application des SLD pour les marqueurs geojson
    function map_successGetSld_geojson_markers(req)
    
    Chargement des marqueurs aux formats geojson
    function map_load_geojson_markers()
    
    fonction d'identification des tuiles pour les flux de type SMT
    function map_flux_SMT(bounds)
    
    Initialisation des flux (om_sig_map_flux)
    function map_load_flux(i)
    
    Chargement des flux de type base
    function map_load_bases_layers()
        fonction qui charge les fonds osm, bing, sat ou un numéro de flux
    
    vide une des listes du selecteur (div map-layers)
    (dest: baselayers, overlays, datas, markers)
    function map_empty_layer_list(dest) 
    
    Chargement des flux de type overlays
    function map_load_overlays()
    
    Chargement des flux de type cart
    function map_load_carts()
    
    Affichage de la couche de base sélectionnée
    function map_display_base_layer()
    
    Ajoute du control openlayers de géolocalisation
    function map_addGeolocateControl()
    
    Ajoute le controle openLayers de sélection
    function map_add_SelectControl()
    
    Traitement des controles de mesure
    function map_handleMeasurements(event)
    
    Ajoute des controles de mesure
    function map_add_MeasureControls()
    
    Ajoute les contrôles openLayers à la carte
    function map_add_controls()
    
    Initialisation de la carte
    function map_init()
    
    
affichage en onglet  (jlb) ::

    affiche_aide
    affiche_layers
    affiche_tools
    affiche_baselayers
    affiche_getfeatures
    
A voir ::

    pb d affichage info si utilisation boite a outil : navigation ou geoloc ou
    mesure distance ou mesure aire.
    

.. _sig_composants:

==========
Composants
==========

Attention la base de données  om_sig_* a changée dans la version 4.4.5
(voir intégration 4.4.0 vers 4.4.5).

* ``OM_ROUTE_MAP``
* ``application::view_map()``
* ``application::get_inst__om_map()``
* :file:`core/om_map.class.php`


Il est necessaire que l'API openLayers soit dans le framework (il y est de base):

lib/openlayers


.. _sig_database:

=================
Modèle de données
=================

.. image:: framework-sig-schema.png


